package ru.aushakov.tm.api.service;

import ru.aushakov.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
